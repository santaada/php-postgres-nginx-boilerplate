# PHP + Postgres + Nginx

## Setup
- Initialzie symfony in `./symfony`
  - `symfony new`
- Install required packages
  - `composer install`
- Generate certificates to support HTTPS on localhost 
  - `cd docker/nginx/certs`
  - `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-selfsigned.key -out nginx-selfsigned.crt`

- Run app via Docker
  - `docker compose up -d`
- Use in-container shell
  - `docker compose exec php-fpm bash`

## Tools
- Static analysis (PHPStan)
  - don't forget to install PHPStan via Composer
  - `vendor/bin/phpstan analyse src --level max`
